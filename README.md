# Camelot Global Instant Win Demo Site

This is a **static** site that is compiled using [Grunt], via which **JSON** data is interpolated to populate the game content.

Listed below are the tools used:

- [Assemble.io]   - for templating and assemblage of static html files
- [Coffeescript]  - for scripting
- [Grunt]         - for task automation/compilation
- [jQuery]        - for DOM manipulation
- [lazysizes]     - for lazyloading
- [SASS]          - for styling
- [Swiper.js]     - for carousel/swiper functionality

As well as all the game data, I have also abstracted out some other data and content into *.json* files, which can be found as below:

```sh
src/
|── data/
    |── config.json
    |── games.json
    |── home-slider.json
    └── why-camelot.json
```

The most critical of these being **games.json**, which is the only file that should require updating.

### Updating the site:
**1.**  Adhering strictly to the JSON object structure set up within the file, update the *games.json* file: ```/src/data/games.json```

**2.**  Assuming you have Node installed, in shell terminal *cd* into the project directory, then run:
```sh
> npm install
```

**3.**  Then, to compile the site, run:
```sh
> grunt deploy
```

**4.**  This should create a **/pub** folder within the project directory, the contents of which can be uploaded to the LAMP server.

___

### Other things to note:
- **config.json** contains some critical site variables that I decided to abstract out of the source code, for example; the site access password, the Google Analytics tracking ID, and the contact email address. If these need to change - this is where you do it.
- The site will currently only support a maximum of **FOUR** featured games. Featured games are defined within **games.json** via the "featured" property which must be set to *true* or *false* (boolean, not a string). Even if more than four games are set to *true*, only the first four will appear on the site.
- The *price-range* property in **games.json** defines the number of euro symbols that appear on the game details page. This **must** be an integer, not a string, from 1 - 5. Anything, else will default to **TWO** euro symbols. Floats or decimals are not supported.
- If the *small-print* property is defined as an empty string, that section will be removed from the summary section.
- As of 18-07-2016 (as per *David Moggs*), the descriptor strings for the game preview slider have been removed. Steps to reinstate these are given in the partials within comments adjacent to the area affected.
- For any given game, setting the *demo-availability* property to **true** assumes that the game source code exists inside the */demos/* folder. The source code itself must be inside a folder named exactly the same as the *slug* property.
- In order for the user to be redirected to the game page following the game demo, the **endUrl** property must be updated to follow this structure: "/demo-redirect.php?game=[GAME_SLUG]", e.g. "/demo-redirect.php?game=monopoly-gold". The referencing and initiatiation of this variable is inconsistent between games.



[//]: # (Reference links:)
   [Assemble.io]: <http://assemble.io/>
   [SASS]: <http://sass-lang.com/>
   [Coffeescript]: <http://coffeescript.org/>
   [Grunt]: <http://gruntjs.com/>
   [jQuery]: <https://jquery.com/>
   [Swiper.js]: <http://idangero.us/swiper/>
   [lazysizes]: <https://github.com/aFarkas/lazysizes>
