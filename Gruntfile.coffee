_     = require('lodash')
path  = require('path')

module.exports = (grunt) ->

  # Function for creating an array of files from each node in a JSON file
  # This is a static hack in lieu of using a dynamic framework approach...
  createPages = (datasrc, template, suffix) =>
    game_data = grunt.file.readJSON(datasrc)
    pageArray = []

    for i, game of game_data
      pageArray.push {
        filename: game.slug
        data:
          title: game.name + suffix
          game: game
        content: grunt.file.read(template)
      }
    return _.flatten(pageArray)

  grunt.initConfig

    # ::: BASE TASKS :::

    # Compiles SASS to CSS
    sass:
      compile:
        options:
          style: 'expanded'
        files:
          'pub/css/styles.css': 'src/sass/manifest.sass'

    # Compiles Coffee to JS
    coffee:
      compile:
        expand: true,
        flatten: true,
        cwd: 'src/js',
        src: ['**/*.coffee'],
        dest: 'src/js/compiled',
        ext: '.js'

    concat:
      dist:
        src: ['src/js/compiled/*.js']
        dest: 'pub/js/script.js'

    # Copies images from src/img to pub/img
    copy:
      images:
        expand: true
        cwd: 'src/img/'
        src: ['**/*.{png,jpg,gif,svg}']
        dest: 'pub/img'
      php_classes:
        expand: true
        cwd: 'src/php/'
        src: ['**/*.{php,json}']
        dest: 'pub'
      demo_games:
        expand: true
        cwd: 'src/demos/'
        src: ['**/*']
        dest: 'pub/demos'


    # Compiles .PHP files using Assemble.io
    assemble:
      options:
        assets: 'pub/img'
        partials: ['src/templates/partials/*.hbs']
        layout: ['src/templates/layouts/main.hbs']
        data: ['src/data/*.{json,yml}']
        ext: '.php'
        removeHbsWhitespace: true
      # Basically index.php and demo-redirect.php
      static:
        expand: true
        cwd: 'src/templates/pages/'
        src: ['*.hbs','!game.hbs','!play.hbs','!login.hbs','!404.hbs']
        dest: 'pub/'
      # A page per game in pub/games/
      game_pages:
        options:
          pages: createPages('src/data/games.json', 'src/templates/pages/game.hbs', ' | Camelot Games')
        files: [
          { dest: 'pub/games/', src: '!*' }
        ]
      # A page per game in pub/play/
      play_pages:
        options:
          pages: createPages('src/data/games.json', 'src/templates/pages/play.hbs', ' | Camelot Games')
        files: [
          { dest: 'pub/play/', src: '!*' }
        ]
      # The login page
      auth:
        options:
          layout: ['src/templates/layouts/auth.hbs']
        expand: true
        cwd: 'src/templates/pages/'
        src: ['login.hbs']
        dest: 'pub'
      # The 404 page
      four_oh_four:
        options:
          layout: ['src/templates/layouts/404.hbs']
        expand: true
        cwd: 'src/templates/pages/'
        src: ['404.hbs']
        dest: 'pub'

    # Prettifies html files
    prettify:
      all:
        expand: true
        cwd: 'pub'
        ext: '.php'
        src: ['**/*.php']
        dest: 'pub/'

    # Watch SASS, Coffee, HBS, *.json, and images
    watch:
      styles:
        files: 'src/**/*.sass'
        tasks: ['sass']
      scripts:
        files: 'src/**/*.coffee'
        tasks: ['coffee', 'concat']
      html:
        files: ['src/**/*.hbs']
        tasks: ['assemble']
      data:
        files: ['src/data/**/*.json']
        tasks: ['assemble']
      images:
        files: ['src/img/**/*.{png,jpg,gif,svg}']
        tasks: ['copy:images']
      php:
        files: ['src/php/**/*.{php,json}']
        tasks: ['copy:php_classes']
      demo_games:
        files: ['src/php/**/*.{html,php,json}']
        tasks: ['copy:demo_games']


    # Deploy specific tasks
    cssmin:
      options:
        shorthandCompacting: false,
        roundingPrecision: -1
      target:
        files:
          'pub/css/styles.css': ['pub/css/styles.css']
    uglify:
      public:
        files:
          'pub/js/script.js': ['pub/js/script.js']
    imagemin:
      files:
        expand: true
        cwd: 'pub/img'
        src: ['**/*.{png,jpg,gif,svg}']
        dest: 'pub/img'
  
  # Load Tasks
  grunt.loadNpmTasks 'grunt-assemble'
  grunt.loadNpmTasks 'grunt-contrib-coffee'
  grunt.loadNpmTasks 'grunt-contrib-concat'
  grunt.loadNpmTasks 'grunt-contrib-copy'
  grunt.loadNpmTasks 'grunt-contrib-cssmin'
  grunt.loadNpmTasks 'grunt-contrib-imagemin'
  grunt.loadNpmTasks 'grunt-contrib-sass'  
  grunt.loadNpmTasks 'grunt-contrib-uglify'
  grunt.loadNpmTasks 'grunt-contrib-watch'

  # Default task(s).
  grunt.registerTask 'default', ['sass', 'coffee', 'concat', 'copy', 'assemble']
  grunt.registerTask 'watchzzz',   ['default', 'watch']

  # Delpoy task(s).
  grunt.registerTask 'deploy',  ['default', 'cssmin', 'uglify', 'imagemin']

