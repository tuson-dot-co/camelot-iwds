class Global

    constructor: ->

        @initDom()

        window.lazySizesConfig =
            addClasses: true

        # It's business time
        $(document).ready =>

            # Initialize a new Swiper.js slider
            @whyCamelotSlider = new Swiper @why_camelot_slider, {
                spaceBetween: 0
                speed: 600 # Duration of transition between slides
                loop: true
                autoplay: 3800 # Delay between transitions
                autoplayDisableOnInteraction: false
                effect: 'slide'
                pagination: '.why-camelot__pagination'
                paginationClickable: false
            }

            @contact_anchor.on 'click', @doScroll

    doScroll: (e) =>

        $('html, body').animate({
            scrollTop: $("#learn-more").offset().top
        }, 600)

    initDom: =>
        @why_camelot_slider = $('.why-camelot__slides-container')

        @contact_anchor = $('#header__contact-anchor')


@Global = new Global