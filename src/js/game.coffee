class Game

    constructor: ->

        @initDom()

        @appendGameCss()

        $(document).ready => 

            @gamePreviewSlider = new Swiper @gamePreviewSliderContainer, {
                spaceBetween: 0
                speed: 600 # Duration of transition between slides
                loop: true
                autoplay: 3800 # Delay between transitions
                autoplayDisableOnInteraction: false
                effect: 'slide'
                pagination: '.game-preview--pagination'
                paginationClickable: if @isMobile() then false else true
            }

    appendGameCss: => 

        css = document.createElement 'style'
        css.type = 'text/css'
        gameFgColour = @gamePreviewSliderPagination.attr 'data-fg-colour'

        styles = ".swiper-pagination.game-preview--pagination .swiper-pagination-bullet-active { background-color: #{gameFgColour} !important}"

        if css.styleSheet
            css.styleSheet.cssText = styles
        else
            css.appendChild(document.createTextNode(styles))

        document.getElementsByTagName("head")[0].appendChild(css)


    isMobile: ->
        (/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test( navigator.userAgent.toLowerCase() ))



    initDom: =>

        @gamePreviewSliderContainer = $('#slider--game-preview')
        @gamePreviewSliderPagination = $('.game-preview--pagination')

@Game = new Game