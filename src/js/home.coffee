class Homepage

    constructor: ->

        @initDom()

        # It's business time
        window.onload = =>

            @heroWrapper.addClass 'visible'

            # Initialize a new Swiper.js slider
            @homepageHero = new Swiper @hero_slider, {
                spaceBetween: 0
                speed: 600 # Duration of transition between slides
                loop: true
                autoplay: 3800 # Delay between transitions
                autoplayDisableOnInteraction: false
                effect: if @isMobile() then 'slide' else 'fade'
                pagination: '.swiper-pagination'
                paginationClickable: true
            }



    isMobile: ->
        (/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test( navigator.userAgent.toLowerCase() ))

    initDom: =>
        @heroWrapper = $('.hero--wrapper')
        @hero_slider = $('#home .hero--slider')


@Homepage = new Homepage