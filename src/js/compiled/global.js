(function() {
  var Global,
    bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  Global = (function() {
    function Global() {
      this.initDom = bind(this.initDom, this);
      this.doScroll = bind(this.doScroll, this);
      this.initDom();
      window.lazySizesConfig = {
        addClasses: true
      };
      $(document).ready((function(_this) {
        return function() {
          _this.whyCamelotSlider = new Swiper(_this.why_camelot_slider, {
            spaceBetween: 0,
            speed: 600,
            loop: true,
            autoplay: 3800,
            autoplayDisableOnInteraction: false,
            effect: 'slide',
            pagination: '.why-camelot__pagination',
            paginationClickable: false
          });
          return _this.contact_anchor.on('click', _this.doScroll);
        };
      })(this));
    }

    Global.prototype.doScroll = function(e) {
      return $('html, body').animate({
        scrollTop: $("#learn-more").offset().top
      }, 600);
    };

    Global.prototype.initDom = function() {
      this.why_camelot_slider = $('.why-camelot__slides-container');
      return this.contact_anchor = $('#header__contact-anchor');
    };

    return Global;

  })();

  this.Global = new Global;

}).call(this);
