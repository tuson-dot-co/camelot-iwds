(function() {
  var Homepage,
    bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  Homepage = (function() {
    function Homepage() {
      this.initDom = bind(this.initDom, this);
      this.initDom();
      window.onload = (function(_this) {
        return function() {
          _this.heroWrapper.addClass('visible');
          return _this.homepageHero = new Swiper(_this.hero_slider, {
            spaceBetween: 0,
            speed: 600,
            loop: true,
            autoplay: 3800,
            autoplayDisableOnInteraction: false,
            effect: _this.isMobile() ? 'slide' : 'fade',
            pagination: '.swiper-pagination',
            paginationClickable: true
          });
        };
      })(this);
    }

    Homepage.prototype.isMobile = function() {
      return /android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase());
    };

    Homepage.prototype.initDom = function() {
      this.heroWrapper = $('.hero--wrapper');
      return this.hero_slider = $('#home .hero--slider');
    };

    return Homepage;

  })();

  this.Homepage = new Homepage;

}).call(this);
