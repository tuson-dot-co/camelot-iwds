(function() {
  var Game,
    bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  Game = (function() {
    function Game() {
      this.initDom = bind(this.initDom, this);
      this.appendGameCss = bind(this.appendGameCss, this);
      this.initDom();
      this.appendGameCss();
      $(document).ready((function(_this) {
        return function() {
          return _this.gamePreviewSlider = new Swiper(_this.gamePreviewSliderContainer, {
            spaceBetween: 0,
            speed: 600,
            loop: true,
            autoplay: 3800,
            autoplayDisableOnInteraction: false,
            effect: 'slide',
            pagination: '.game-preview--pagination',
            paginationClickable: _this.isMobile() ? false : true
          });
        };
      })(this));
    }

    Game.prototype.appendGameCss = function() {
      var css, gameFgColour, styles;
      css = document.createElement('style');
      css.type = 'text/css';
      gameFgColour = this.gamePreviewSliderPagination.attr('data-fg-colour');
      styles = ".swiper-pagination.game-preview--pagination .swiper-pagination-bullet-active { background-color: " + gameFgColour + " !important}";
      if (css.styleSheet) {
        css.styleSheet.cssText = styles;
      } else {
        css.appendChild(document.createTextNode(styles));
      }
      return document.getElementsByTagName("head")[0].appendChild(css);
    };

    Game.prototype.isMobile = function() {
      return /android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase());
    };

    Game.prototype.initDom = function() {
      this.gamePreviewSliderContainer = $('#slider--game-preview');
      return this.gamePreviewSliderPagination = $('.game-preview--pagination');
    };

    return Game;

  })();

  this.Game = new Game;

}).call(this);
